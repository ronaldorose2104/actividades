def Bocas_Del_Toro():
    while True:
        print("Bocas Del Toro cuenta con 4 Distritos")
        print("|Almirante    |Bocas Del Toro")
        print("|Changuinola  |Chiriqí Grande")
        print()
        try:
            opcion = int(input("Pesione 1 para regresar"))
        except:
            opcion = -1
        if opcion == 1:
            print()
            break
        else:
            print("ERROR")

def Chiriqui():
    while True:
        print("Chiriqí cuenta con 14 Distritos")
        print("|Alamje        |Barú")
        print("|Boquerón      |Boquete")
        print("|Bugaba        |David")
        print("|Dolega        |Gualaca")
        print("|Remedios      |Renacimiento")
        print("|San Félix     |San Lorenzo")
        print("|Tierras Altas |Tolé")
        print()
        try:
            opcion = int(input("Pesione 1 para regresar"))
        except:
            opcion = -1
        if opcion == 1:
            print()
            break
        else:
            print("ERROR")

def Cocle():
    while True:
        print()
        print("|Coclé cuenta con 6 Distritos")
        print("|Aguadulce   |Antón")
        print("|La Pintada  |Natá")
        print("|Olá         |Penonomé")
        print()
        try:
            opcion = int(input("Pesione 1 para regresar"))
        except:
            opcion = -1
        if opcion == 1:
            print()
            break
        else:
            print("ERROR")

def Colon():
    while True:
        print("Colón cuenta con 6 Distritos")
        print("|Chagres      |Colón")
        print("|Donoso       |Portobelo")
        print("|Santa Isabel |Omar Torrijos Herrera")
        print()
        try:
            opcion = int(input("Pesione 1 para regresar"))
        except:
            opcion = -1
        if opcion == 1:
            print()
            break
        else:
            print("ERROR")

def Darien():
    while True:
        print("Darién cuenta con 3 Distritos")
        print("|Chepigana   |Pinogana")
        print("|Santa Fe")
        print()
        try:
            opcion = int(input("Pesione 1 para regresar"))
        except:
            opcion = -1
        if opcion == 1:
            print()
            break
        else:
            print("ERROR")

def Herrera():
    while True:
        print("Herrera cuenta con 7 Distritos")
        print("|Chitré       |Las Minas")
        print("|Los Pozos    |Ocú")
        print("|Parita       |Pesé")
        print("|Santa Maria")
        print()
        try:
            opcion = int(input("Pesione 1 para regresar"))
        except:
            opcion = -1
        if opcion == 1:
            print()
            break
        else:
            print("ERROR")

def Los_Santos():
    while True:
        print("Los Santos cuenta con 7 Distritos")
        print("|Guararé     |Las Tablas")
        print("|Loas Santos |Macaracas")
        print("|Pedasí      |Pocorí")
        print("|Tonosí")
        print()
        try:
            opcion = int(input("Pesione 1 para regresar"))
        except:
            opcion = -1
        if opcion == 1:
            print()
            break
        else:
            print("ERROR")

def Panama():
    while True:
        print("Panama cuenta con 6 Distritos")
        print("|Balboa        |Chepo")
        print("|Chimán        |Panamá")
        print("|San Miguelito |Taboga")
        print()
        try:
            opcion = int(input("Pesione 1 para regresar"))
        except:
            opcion = -1
        if opcion == 1:
            print()
            break
        else:
            print("ERROR")

def Panama_Oeste():
    while True:
        print("Panamá Oeste cuenta con 5 Distritos")
        print("|Arraiján     |Capira")
        print("|Chame        |La Chorrera")
        print("|San Carlos")
        print()
        try:
            opcion = int(input("Pesione 1 para regresar"))
        except:
            opcion = -1
        if opcion == 1:
            print()
            break
        else:
            print("ERROR")

def Veraguas():
    while True:
        print("Veraguas cuenta con 12 Distritos")
        print("|Atalaya       |Calobre")
        print("|Cañazas       |La Mesa")
        print("|Las Palmas    |Mariato")
        print("|Montijo       |Río de Jesús")
        print("|San Francisco |Santa Fe")
        print("|Santiago      |Soná")
        print()
        try:
            opcion = int(input("Pesione 1 para regresar"))
        except:
            opcion = -1
        if opcion == 1:
            print()
            break
        else:
            print("ERROR")

while True:
    print("Menu")
    print("1. Bocas Del Toro")
    print("2. Chiriqí")
    print("3. Coclé")
    print("4. Colón")
    print("5. Darién")
    print("6. Herrera")
    print("7. Los Santo")
    print("8. Panamá")
    print("9. Panamá Oeste")
    print("10. Veraguas")
    print("11. Salir")
    print()
    try:
        opcion = int(input("Secleccione la provincia :: "))
        print()
    except:
        opcion = -1
    if opcion == 1:
        Bocas_Del_Toro()
    elif opcion == 2:
        Chiriqui()
    elif opcion == 3:
        Cocle()
    elif opcion == 4:
        Colon()
    elif opcion == 5:
        Darien()
    elif opcion == 6:
        Herrera()
    elif opcion == 7:
        Los_Santos()
    elif opcion == 8:
        Panama()
    elif opcion == 9:
        Panama_Oeste()
    elif opcion == 10:
        Veraguas()
    elif opcion == 11:
        print()
        break
    else:
        print("ERROR")
print("Chao")

